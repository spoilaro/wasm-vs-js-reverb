# WASM vs JS Reverb Processing

This repository contains reverb implementations for both vanilla JS and reverb. The aim of this project to was research performance differences between JS and WASM in audio-processing.

** NOTE:** The code structure is forked from Peter Suggate's WASM pitch detector.
https://github.com/peter-suggate/wasm-audio-app

# Quick Start: Cloning, Building, and Running

Assuming prerequisites are installed, the following steps should get the app running on your system:

``` sh
cd wasm-audio-app/wasm-audio/
wasm-pack build --target web
cd ..
cp -R ./wasm-audio/pkg ./public/wasm-audio
npm install
npm start
```
# Switching between JS and WASM

`App.js` has a section of code described below. To enable other reverb implementation,
un-comment that `setAudio` call and comment the current one.

`setupAudio` => WASM implementation
`setupJSAudio` => JS implementation

``` js
  if (!audio) {
    return (
      <button
        onClick={async () => {
          setAudio(await setupAudio());
          //setAudio(await setupJSAudio());
          setRunning(true);
        }}
      >
        Start listening
      </button>
    );
  }

```
