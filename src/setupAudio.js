import ReverbNode from "./ReverbNode";

async function getWebAudioMediaStream() {
  if (!window.navigator.mediaDevices) {
    throw new Error(
      "This browser does not support web audio or it is not enabled."
    );
  }

  try {
    const result = await window.navigator.mediaDevices.getUserMedia({
      audio: true,
      video: false,
    });

    return result;
  } catch (e) {
    switch (e.name) {
      case "NotAllowedError":
        throw new Error(
          "A recording device was found but has been disallowed for this application. Enable the device in the browser's settings."
        );

      case "NotFoundError":
        throw new Error(
          "No recording device was found. Please attach a microphone and click Retry."
        );

      default:
        throw e;
    }
  }
}

export async function setupJSAudio() {

  const mediaStream = await getWebAudioMediaStream();

  const context = new window.AudioContext();
  const audioSource = context.createMediaStreamSource(mediaStream);

  const processorUrl = "VanillaProcessor.js";
  try {
    await context.audioWorklet.addModule(processorUrl);
  } catch (e) {
    throw new Error(
      `Failed to load audio analyzer worklet at url: ${processorUrl}. Further info: ${e.message}`
    );
  }

  // Create the AudioWorkletNode which enables the main Javascript thread to
  // communicate with the audio processor (which runs in a Worklet).
  let vanillaNode = new AudioWorkletNode(context, "vanilla-reverb");

  audioSource.connect(vanillaNode).connect(context.destination);

  return {context, vanillaNode}
}

export async function setupAudio() {
  // Get the browser's audio. Awaits user "allowing" it for the current tab.
  const mediaStream = await getWebAudioMediaStream();

  const context = new window.AudioContext();
  const audioSource = context.createMediaStreamSource(mediaStream);

  let node;

  try {
    const response = await window.fetch("wasm-audio/wasm_audio_bg.wasm");
    const wasmBytes = await response.arrayBuffer();

    // Add our audio processor worklet to the context.
    const processorUrl = "ReverbProcessor.js";
    try {
      await context.audioWorklet.addModule(processorUrl);
    } catch (e) {
      throw new Error(
        `Failed to load audio analyzer worklet at url: ${processorUrl}. Further info: ${e.message}`
      );
    }

    // Create the AudioWorkletNode which enables the main Javascript thread to
    // communicate with the audio processor (which runs in a Worklet).
    node = new ReverbNode(context, "ReverbProcessor");

    const numAudioSamplesPerAnalysis = 1024;

    // Send the Wasm module to the audio node which in turn passes it to the
    // processor running in the Worklet thread. Also, pass any configuration
    // parameters for the Wasm detection algorithm.
    node.init(wasmBytes, numAudioSamplesPerAnalysis);

    // Connect the audio source (microphone output) to our analysis node.
    // audioSource.connect(node).connect(context.destination);
    audioSource.connect(node)
    // Connect our analysis node to the output. Required even though we do not
    // output any audio. Allows further downstream audio processing or output to
    // occur.
    node.connect(context.destination);
  } catch (err) {
    throw new Error(
      `Failed to load audio analyzer WASM module. Further info: ${err.message}`
    );
  }

  return { context, node };
}
