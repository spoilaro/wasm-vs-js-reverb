use wasm_bindgen::prelude::*;
mod utils;


#[wasm_bindgen]
pub struct WasmReverb {
  sample_rate: usize,
  fft_size: usize,
}

#[wasm_bindgen]
impl WasmReverb {
  pub fn new(sample_rate: usize, fft_size: usize) -> WasmReverb {
    utils::set_panic_hook();

    // Define the amount of zero-padding applied to each analysis FFT. Padding,
    // in combination with the windowing function used by the algorithm helps
    // "smooth" the results as the analysis moves across the incoming sampled audio
    // data. Using a pad of half the fft length works well for many instruments.
    let fft_pad = fft_size / 2;

    WasmReverb {
      sample_rate,
      fft_size,
    }
  }

  pub fn process(&mut self, audio_samples: Vec<f32>) -> Vec<f32> {
    return audio_samples;
  }

  #[wasm_bindgen]
  //pub fn process_audio(sample_rate: i32, input_channel: js_sys::Uint32Array, output_channel: js_sys::Uint32Array) -> js_sys::Uint32Array {
  pub fn process_audio(
    &mut self,
    sample_rate: i32,
    input_channel: Vec<f32>
  ) -> Vec<f32> {
    // Seconds
    let reverb_duration: i32 = 3;
    let reverb_length = sample_rate * reverb_duration;
    let mut reverb_buffer: Vec<i32> = (0..reverb_length).map(|v| v).collect();

    let mut output_channel: Vec<f32> = vec![0.0; input_channel.len()];

    let dry: f32 = 0.5;
    let wet: f32 = 0.2;
    let room_size: f32 = 0.5;

    let delay_time = room_size * 2.0;

    let delay_samples = delay_time as i32 * sample_rate;

    let mut write_index = 0;
    write_index = (write_index + input_channel.len() as i32) % reverb_buffer.len() as i32;

    let mut read_index = 0;
    read_index =
      (write_index + reverb_buffer.len() as i32 - delay_samples) % reverb_buffer.len() as i32;

    for i in 0..input_channel.len() {
      let reverb_sample = input_channel[read_index as usize];
      output_channel[i] = input_channel[i] * dry + reverb_sample * wet;

      reverb_buffer[write_index as usize] =
        (input_channel[i]* dry + reverb_sample * wet) as i32;

      read_index = (read_index + 1) % reverb_buffer.len() as i32;
      write_index = (write_index + 1) % reverb_buffer.len() as i32;
    }

    return output_channel;
  }
}
